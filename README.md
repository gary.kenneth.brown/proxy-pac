# proxy-pac

Automates the generation of a proxy-pac file for use with my personal devices. The proxy pac will allow any device to cache general web browsing, downloads, and build artifacts (Maven, NPM) via the local proxy whilst allowing recommended (such as Windows updates) and video streams (MS Teams, MS Streams, Youtube) to be a DIRECT connection.

* O365 Worldwide - DIRECT
* M365 endpoints - DIRECT
* everything else - PROXY

## Execution

To generate a new proxy-pac, run the command:

```

node .

```

This will generate a new `proxy.pac` in the current working directory.


## Windows 10 users

Windows 10 no longer supports proxy.pac files using the `file://` protocol. You will need to deploy using `http://` such as Apache2 or Nginx.

## Improvements

1. Parameterise the proxy url and port.
2. Other DIRECT