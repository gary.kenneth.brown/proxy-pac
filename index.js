'use strict';
const https = require('https');
const fs = require('fs');

//
// https://docs.microsoft.com/en-us/microsoft-365/enterprise/microsoft-365-ip-web-service?view=o365-worldwide
//

var url = 'https://endpoints.office.com/endpoints/worldwide?clientrequestid=b10c5ed1-bad1-445f-b386-b919946339a7&format=json';

const $PROXY_SERVER = '192.168.0.254'

var matchUrls = ["*.service.nhsbsa", "*.lync.com", "*.teams.microsoft.com", "teams.microsoft.com", "*.broadcast.skype.com", "broadcast.skype.com", "*.sfbassets.com", "*.keydelivery.mediaservices.windows.net", "*.msecnd.net", "*.streaming.mediaservices.windows.net", "ajax.aspnetcdn.com", "mlccdn.blob.core.windows.net", "aka.ms", "amp.azure.net", "*.users.storage.live.com", "*.adl.windows.com", "*.skypeforbusiness.com", "*.msedge.net", "compass-ssl.microsoft.com", "*.mstea.ms", "*.secure.skypeassets.com", "mlccdnprod.azureedge.net", "videoplayercdn.osi.office.net", "*.tenor.com", "*.skype.com", "ajax.aspnetcdn.com", "r3.res.outlook.com", "spoprod-a.akamaihd.net", "*.microsoftstream.com", "amp.azure.net", "s0.assets-yammer.com", "vortex.data.microsoft.com", "amsglob0cdnstream13.azureedge.net", "amsglob0cdnstream14.azureedge.net", "nps.onyx.azure.net", "*.azureedge.net", "*.media.azure.net", "*.streaming.mediaservices.windows.net", "*.keydelivery.mediaservices.windows.net", "*.streaming.mediaservices.windows.net", "*.officeapps.live.com", "*.online.office.com", "office.live.com", "*.cdn.office.net", "contentstorage.osi.office.net", "*.onenote.com", "*.microsoft.com", "*.msecnd.net", "*.office.net", "*cdn.onenote.net", "ad.atdmt.com", "s.ytimg.com", "ajax.aspnetcdn.com", "apis.live.net", "cdn.optimizely.com", "officeapps.live.com", "www.onedrive.com", "*.msftidentity.com", "*.msidentity.com", "account.activedirectory.windowsazure.com", "accounts.accesscontrol.windows.net", "adminwebservice.microsoftonline.com", "api.passwordreset.microsoftonline.com", "autologon.microsoftazuread-sso.com", "becws.microsoftonline.com", "clientconfig.microsoftonline-p.net", "companymanager.microsoftonline.com", "device.login.microsoftonline.com", "graph.microsoft.com", "graph.windows.net", "login.microsoft.com", "login.microsoftonline.com", "login.microsoftonline-p.com", "login.windows.net", "logincert.microsoftonline.com", "loginex.microsoftonline.com", "login-us.microsoftonline.com", "nexus.microsoftonline-p.com", "passwordreset.microsoftonline.com", "provisioningapi.microsoftonline.com", "*.hip.live.com", "*.microsoftonline.com", "*.microsoftonline-p.com", "*.msauth.net", "*.msauthimages.net", "*.msecnd.net", "*.msftauth.net", "*.msftauthimages.net", "*.phonefactor.net", "enterpriseregistration.windows.net", "management.azure.com", "policykeyservice.dc.ad.msft.net", "*.compliance.microsoft.com", "*.protection.office.com", "*.security.microsoft.com", "compliance.microsoft.com", "protection.office.com", "security.microsoft.com", "*.portal.cloudappsecurity.com", "account.office.net", "suite.office.net", "*.blob.core.windows.net", "*.aria.microsoft.com", "*.events.data.microsoft.com", "*.o365weve.com", "amp.azure.net", "appsforoffice.microsoft.com", "assets.onestore.ms", "auth.gfx.ms", "c1.microsoft.com", "contentstorage.osi.office.net", "dgps.support.microsoft.com", "docs.microsoft.com", "msdn.microsoft.com", "platform.linkedin.com", "prod.msocdn.com", "shellprod.msocdn.com", "support.content.office.net", "support.microsoft.com", "technet.microsoft.com", "videocontent.osi.office.net", "videoplayercdn.osi.office.net", "*.office365.com", "*.cloudapp.net", "*.aadrm.com", "*.azurerms.com", "*.informationprotection.azure.com", "ecn.dev.virtualearth.net", "informationprotection.hosting.portal.azure.net"];

function writePac(serviceUrls) {
	console.log("response: ", serviceUrls.length);
	
	var pacFileString = "// this is a generated file, do not edit\n\n";
	var prefix = "";
	var matchString = "";
	for(var i = 0; i < serviceUrls.length; i++) {
		matchString += prefix + 'shExpMatch(host, "'+ serviceUrls[i] +'")';
		if(i == 0) prefix = "    || ";
	}

	pacFileString += "function FindProxyForURL(url, host) {\n";
	pacFileString += '    var direct = "DIRECT";\n';
	pacFileString += '    var proxyServer = "PROXY ' + $PROXY_SERVER + ':3128";\n';
	pacFileString += "    host = host.toLowerCase();\n";
	pacFileString += "    if (" + matchString + ") {\n";
	pacFileString += "        return direct;\n";
	pacFileString += "    }\n";
	pacFileString += "    return proxyServer;\n"
	pacFileString += "}\n"
	
	fs.writeFileSync('proxy.pac', pacFileString);
};



https.get(url, function(res){
    var body = '';

    res.on('data', function(chunk){
        body += chunk;
    });

    res.on('end', function(){
        var serviceUrls = JSON.parse(body);
		console.log("Services: ", serviceUrls.length);
		
		// flatten urls to a single array
		for(var i = 0; i < serviceUrls.length; i++) {
			console.log("service: ", serviceUrls[i].id, serviceUrls[i].urls);
			for(var j = 0; serviceUrls[i].urls && j < serviceUrls[i].urls.length; j++) {
			    matchUrls.push(serviceUrls[i].urls[j]);
			}
		}
		
        writePac(matchUrls);
    });
}).on('error', function(e){
      console.log("Got an error: ", e);
});